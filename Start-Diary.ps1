Set-StrictMode -Version Latest

$greeting = "DIARY $(Get-Date -Format "yyyy-MM-dd")"
$padding = ($Host.UI.RawUI.WindowSize.Width - 2 - $greeting.Length) / 2
$header = @"
$("#" * $Host.UI.RawUI.WindowSize.Width)
#$(" " * $padding)$greeting$(" " * $padding)#
$("#" * $Host.UI.RawUI.WindowSize.Width)
"@

Clear-Host
Write-Host $header

Write-Host "Loading configuration ..."
try {
    $config = Get-Content -Path "$PSScriptRoot\\Configuration\\config.json" | ConvertFrom-Json -ErrorAction Stop
}
catch {
    $error_msg = $PSItem.Exception.Message
    Write-Error -Message "Failed to load configuration file. Error: $error_msg"
    exit 1
}

Write-Host "Loading dependencies"
try {
    foreach ($assembly in $config.AssemblyFileList) {
        Add-Type -Path $assembly -ErrorAction Stop
    }
    foreach ($tool in $config.PSToolsList) {
        . $tool -ErrorAction Stop
    }
    foreach ($dbfile in $config.DBFileList) {
        Initialize-DBFile -DBInfo $dbfile
    }
}
catch {
    $error_msg = $PSItem.Exception.Message
    Write-Error -Message "Failed to load dependencies. Error: $error_msg"
    exit 1
}

Write-Host "Connecting to log database ..."
try {
    $log_con = New-Object -TypeName System.Data.SQLite.SQLiteConnection -ErrorAction Stop
    $log_con.ConnectionString = "Data Source=$(
        ($config.DBFileList | Where-Object -FilterScript {$_.TableName -eq "LOG"}
    ).FilePath)"
    $log_con.Open()
    Write-Log -DBConnection $log_con -Message "Succesfully initialized app"
}
catch {
    $error_msg = $PSItem.Exception.Message
    Write-Error -Message "Failed to connect to log database. Error: $error_msg"
    [void]$log_con.Dispose()
    exit 1
}

Clear-Host
Write-Host $header
Write-Host @"
Last $(
    if ($config.AppConfig.LastPostHistory -eq 1) {
        "entry"
    }
    else {
        "$($config.AppConfig.LastPostHistory) entries"
    }
):
"@
Write-Host $(Diary-GetLastEntries -LogConnection $log_con -Config $config -Count $config.AppConfig.LastPostHistory)
$option = Diary-DisplayMainMenu

while (1) {
    switch ($option) {
        1 {
            Clear-Host
            Write-Log -DBConnection $log_con -Message "Entered menu $option"
            Write-Host $header
            Diary-NewEntry -LogConnection $log_con -Config $config
            break;
        }
        2 {
            Clear-Host
            Write-Log -DBConnection $log_con -Message "Entered menu $option"
            Write-Host $header
            Write-Host "Diary-ListHistory"
            Read-Host
            break;
        }
        3 {
            Clear-Host
            Write-Log -DBConnection $log_con -Message "Entered menu $option"
            Write-Host $header
            Write-Host "Diary-GetOlderEntry"
            Read-Host
            break;
        }
        4 {
            Clear-Host
            Write-Log -DBConnection $log_con -Message "Entered menu $option"
            Write-Host $header
            Write-Host "Diary-RemoveOlderEntry"
            Read-Host
            break;
        }
        0 {
            Write-Log -DBConnection $log_con -Message "Closing application"
            $log_con.Dispose()
            Clear-Host
            exit 0
        }
        Default {
            Write-Log -DBConnection $log_con -Message "Unkown option choosen '$option'"
            break;
        }
    }
    Clear-Host
    Write-Host $header
    $option = Diary-DisplayMainMenu
}

<#

Menu structure

1. New diary entry
2. List diary history
3. Display older entry
4. Delete diary entry

0. Exit

#>
