function Diary-DisplayMainMenu {
    Write-Host @"

Menu:

1. New diary entry
2. List diary history
3. Display older entry
4. Delete diary entry

0. Exit

"@

    return Read-Host -Prompt "Choose an option"
}

function Diary-GetLastEntries{
    param (
        [Parameter(Mandatory = $true)]
        [object]
        $LogConnection,

        [Parameter(Mandatory = $true)]
        [object]
        $Config,

        [Parameter(Mandatory = $true)]
        [int]
        $Count
    )

    $data = Get-LastDiaryEntry -LogConnection $LogConnection -Config $Config -Count $Count

    if ($data.Tables[0].Rows.Count -eq 0) {
        return "Diary is empty!"
    }
    else {
        $epoch_start = New-Object -TypeName System.DateTime -ArgumentList 1970, 1, 1, 0, 0, 0, 0
        foreach ($row in $data.Tables[0].Rows) {
            $time = $epoch_start.AddSeconds($row.TIMESTAMP)
            Write-Host "[$(Get-Date -Date $time -Format "yyyy/mm/dd HH:MM:ss")]"
            Write-Host $row.TITLE
            Write-Host $row.DATA
            Write-Host
        }
    }
}

function Diary-NewEntry {
    param (
        [Parameter(Mandatory = $true)]
        [object]
        $LogConnection,

        [Parameter(Mandatory = $true)]
        [object]
        $Config
    )

    Write-Host "[$(Get-Date -Format "yyyy/mm/dd HH:MM:ss")]"
    $title = Read-Host -Prompt "Title"
    Write-Host "Text:"
    $data = while (($line = Read-Host) -ne "EOF") {
        $line
    }

    $result = Insert-DiaryEntry -LogConnection $LogConnection `
                                -Config $Config `
                                -Title $title `
                                -Data $($data -join [System.Environment]::NewLine)
    if ($result -eq $false) {
        # handle that data was not written
        # maybe repeat query
    }
}

function Diary-ListHistory {
    
}

function Diary-GetOlderEntry {
    
}

function Diary-RemoveOlderEntry {
    
}
