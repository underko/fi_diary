function Initialize-DBFile {
    param (
        [Parameter(Mandatory = $true)]
        [object]
        $DBInfo
    )

    $new_file = $false

    # check if file exists and if not create it and it's structure
    if (-not (Test-Path -Path $DBInfo.FilePath -PathType Leaf)) {
        try {
            New-Item -Path $DBInfo.FilePath -ItemType File -ErrorAction Stop | Out-Null
            $new_file = $true
        }
        catch {
            $error_msg = $PSItem.Exception.Message
            Write-Error -Message "Failed to create db file $($DBInfo.FilePath). Error: $error_msg"
            exit 1
        }
    }

    try {
        $connection = New-Object -TypeName System.Data.SQLite.SQLiteConnection -ErrorAction Stop
        $connection.ConnectionString = "Data Source=$($DBInfo.FilePath)"
        $connection.Open()
    }
    catch {
        $error_msg = $PSItem.Exception.Message
        Write-Error -Message "Failed to connect to db $($DBInfo.FilePath). Error: $error_msg"
        exit 1
    }

    if ($new_file) {
        $query = $connection.CreateCommand()
        $query.CommandText = @"
CREATE TABLE $($DBInfo.TableName) (
    $(($DBInfo.Columns | ForEach-Object {$_.Name + " " +  $_.Type}) -join ",`n")
)
"@
        try {
            [void]$query.ExecuteNonQuery()
        }
        catch {
            $error_msg = $PSItem.Exception.Message
            Write-Error -Message "Failed to create table structure: $($query.CommandText). Error: $error_msg"
            $connection.Dispose()
            exit 1
        }
    }
    else {
        # todo: verify db structure
    }

    $connection.Dispose()
}

function Get-LastDiaryEntry {
    param (
        [Parameter(Mandatory = $true)]
        [object]
        $LogConnection,

        [Parameter(Mandatory = $true)]
        [object]
        $Config,

        [Parameter(Mandatory = $false)]
        [int]
        $Count = 1
    )

    Write-Log -DBConnection $LogConnection -Message "Getting last $Count diary entries from DB"
    $output = New-Object -TypeName System.Data.DataSet

    try {
        $connection = New-Object -TypeName System.Data.SQLite.SQLiteConnection -ErrorAction Stop
        $connection.ConnectionString = "Data Source=$(($Config.DBFileList | ? {$_.TableName -eq "DATA"}).FilePath)"
        $connection.Open()
    }
    catch {
        $error_msg = $PSItem.Exception.Message
        Write-Log -DBConnection $LogConnection -Severity "ERROR" -Message "Failed to connect to db $($DBInfo.FilePath). Error: $error_msg"
        return $null
    }

    try {
        $query = $connection.CreateCommand()
        $query.CommandText = @"
SELECT * FROM DATA
WHERE DELETED = 0
ORDER BY TIMESTAMP DESC
LIMIT $Count
"@
        $adapter = New-Object -TypeName System.Data.SQLite.SQLiteDataAdapter $query
        [void]$adapter.Fill($output)
    }
    catch {
        $error_msg = $PSItem.Exception.Message
        Write-Log -DBConnection $LogConnection -Severity "ERROR" -Message "Failed to query db $($DBInfo.FilePath). Error: $error_msg"
        return $null
    }

    return $output
}

function Insert-DiaryEntry {
    param (
        [Parameter(Mandatory = $true)]
        [object]
        $LogConnection,

        [Parameter(Mandatory = $true)]
        [object]
        $Config,

        [Parameter(Mandatory = $true)]
        [string]
        $Title,

        [Parameter(Mandatory = $true)]
        [string]
        $Data
    )

    Write-Log -DBConnection $LogConnection -Message "Inserting new diary entry to DB"
    $timestamp = [System.DateTimeOffset]::UtcNow.ToUnixTimeSeconds()

    try {
        $connection = New-Object -TypeName System.Data.SQLite.SQLiteConnection -ErrorAction Stop
        $connection.ConnectionString = "Data Source=$(($Config.DBFileList | ? {$_.TableName -eq "DATA"}).FilePath)"
        $connection.Open()
    }
    catch {
        $error_msg = $PSItem.Exception.Message
        Write-Log -DBConnection $LogConnection -Severity "ERROR" -Message "Failed to connect to db $($DBInfo.FilePath). Error: $error_msg"
        return $false
    }

    $query = $connection.CreateCommand()
    $query.CommandText = @"
INSERT INTO DATA (
    'TIMESTAMP',
    'TITLE',
    'DATA',
    'DELETED'
)
VALUES (
    @timestamp,
    @title,
    @data,
    @delete
)
"@
    try {
        [void]$query.Parameters.AddWithValue("@timestamp", $timestamp)
        [void]$query.Parameters.AddWithValue("@title", $Title)
        [void]$query.Parameters.AddWithValue("@data", $Data)
        [void]$query.Parameters.AddWithValue("@delete", 0)

        [void]$query.ExecuteNonQuery()
    }
    catch {
        $error_msg = $PSItem.Exception.Message
        Write-Log -DBConnection $LogConnection -Severity "ERROR" -Message "Failed to insert new diary entry. Error: $error_msg"
        return $false
    }

    return $true
}