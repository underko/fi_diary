function Write-Log {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [object]
        $DBConnection,

        [Parameter(Mandatory = $false)]
        [ValidateSet("INFO", "WARNING", "ERROR")]
        [string]
        $Severity = "INFO",

        [Parameter(Mandatory = $true)]
        [string]
        $Message
    )

    $timestamp = [System.DateTimeOffset]::UtcNow.ToUnixTimeSeconds()

    $query = $DBConnection.CreateCommand()
    $query.CommandText = @"
INSERT INTO LOG (
    TIMESTAMP,
    SEVERITY,
    MESSAGE
)
VALUES (
    @timestamp,
    @severity,
    @message
)
"@
    try {
        [void]$query.Parameters.AddWithValue("@timestamp", $timestamp)
        [void]$query.Parameters.AddWithValue("@severity", $Severity)
        [void]$query.Parameters.AddWithValue("@message", $Message)

        [void]$query.ExecuteNonQuery()
    }
    catch {
        $error_msg = $PSItem.Exception.Message
        $DBConnection.Dispose()
        throw [System.Exception]::new("Failed to execute query $($query.CommandText). Error: $error_msg")
    }
}
